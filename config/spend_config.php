<?php
if (!defined('PAGINATION_PER_PAGE')) {
    define('PAGINATION_PER_PAGE', 20);
}

if (!defined('USER_EMPTY_PROFILE')) {
    define('USER_EMPTY_PROFILE', '---');
}
