<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use App\Repository\CategoryRepository;
use Illuminate\Database\Eloquent\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);


        Validator::extend('categoriesInUser', function ($attribute, $value, $parameters, $validator) {

            if (empty($parameters[0])) {
                return false;
            }

            $repository = app(CategoryRepository::class);

            $categories = $repository->getCategoryFromUser($parameters[0]);

            if (!count($categories) > 0) {
                return false;
            }

            $arrCategory = [];

            foreach ($categories as $category) {
                $arrCategory[] = $category->id;
            }
            if (in_array($value, $arrCategory)) {
                return true;
            }


            return false;
        });

        // register collection
        Builder::macro('whereLike', function ($attributes, string $searchTerm){

            $this->where(function (Builder $query) use ($attributes, $searchTerm){
                
                // Post::whereLike(['name', 'text', 'author.name', 'tags.name'], $searchTerm)->get();
                foreach(array_wrap($attributes) as $attribute)
                {
                    $query->when(
                        str_contains($attribute, '.'),
                        function (Builder $query) use ($attribute, $searchTerm){
                           [$relationName, $relationAttribute] = explode('.',$attribute);
                           
                           $query->orWhereHas($relationName, function (Builder $query) use ( $relationAttribute,$searchTerm){
                               $query->where($relationAttribute, 'LIKE', "%{$searchTerm}%");
                           });
                        },
                        function (Builder $query) use ($attribute, $searchTerm) {
                            $query->orWhere($attribute, 'LIKE', "%{$searchTerm}%");
                        }
                    );
                }
            });
    
            return $this;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
