<?php

namespace App\Providers;

use App\Repository\BasicRepository;
use App\Repository\BasicRepositoryEloquent;
use App\Repository\CategoryRepository;
use App\Repository\CategoryRepositoryEloquent;
use App\Repository\ProfileRepository;
use App\Repository\ProfileRepositoryEloquent;
use App\Repository\RoleRepository;
use App\Repository\RoleRepositoryEloquent;
use App\Repository\UserRepository;
use App\Repository\UserRepositoryEloquent;
use Illuminate\Support\ServiceProvider;
use App\Repository\SpendRepository;
use App\Repository\SpendRepositoryEloquent;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SpendRepository::class, SpendRepositoryEloquent::class);
        $this->app->bind(RoleRepository::class, RoleRepositoryEloquent::class);
        $this->app->bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(ProfileRepository::class, ProfileRepositoryEloquent::class);
        $this->app->bind(BasicRepository::class, BasicRepositoryEloquent::class);

    }
}
