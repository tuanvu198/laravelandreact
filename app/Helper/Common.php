<?php

namespace App\Helper;

trait Common
{
    public static function generateToken($length = 60)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length) . md5(time());
    }
}
