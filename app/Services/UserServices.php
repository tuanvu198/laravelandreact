<?php

namespace App\Services;

use Webpatser\Uuid\Uuid;

class UserServices {
    public function uploadImage($file, $path = 'public/users/avatar')
    {
        $fileName = Uuid::generate().'.'.pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $file->storeAs($path, $fileName);

        return $fileName;
    }
}
