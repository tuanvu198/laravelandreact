<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ROLE_ADMIN = 1;
    const ROLE_MEMBER = 2;
    const ROLE_CUSTOMER = 3;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';


    protected $fillable = ['id', 'name', 'description'];
}
