<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Spend extends Model
{
    const RECEIVE = 1;
    const BUY     = 2;
    /*
     * FILTER SPENDS
     * */
    const YESTERDAY = 1;
    const TODAY     = 0;
    const WEEK      = 7;
    protected $table = 'spends';
    protected $fillable   = ['id','user_id','category_id','type','total'];


    public function getCreatedAtFormatAttribute()
    {
        return Carbon::parse($this->created_at)->format('d-m-Y');
    }
    public function getTypeFormatAttribute()
    {
        if($this->type == self::RECEIVE)
        {
            return trans('messages.spends.receive');
        }else{
            return trans('messages.spends.buy');
        }
    }
    public function getTotalFormatAttribute()
    {
        if($this->type === self::BUY) return '-'.number_format($this->total). ' vnđ';
        return number_format($this->total). ' vnđ';
    }
    public function categories()
    {
        return $this->belongsTo('App\Model\Category','category_id');
    }

}
