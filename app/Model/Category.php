<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable   = ['id','name','user_id'];

    public function getCreatedAtFormatAttribute()
    {
        return Carbon::parse($this->created_at)->format('d-m-Y');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User','user_id','id');
    }

}
