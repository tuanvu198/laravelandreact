<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    const SEX_MALE = 0;
    const SEX_FEMALE = 1;
    const SEX_UNKNOWN = 2;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'profile';

    protected $fillable = ['id', 'first_name', 'last_name', 'user_id', 'image', 'sex'];


    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getSexFormatAttribute()
    {
        if ($this->sex == self::SEX_MALE) {
            return trans('messages.sex.male');
        } elseif ($this->sex == self::SEX_FEMALE) {
            return trans('messages.sex.female');
        }
        return trans('messages.sex.unknown');
    }
}
