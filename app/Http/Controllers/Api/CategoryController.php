<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Repository\SpendRepository;
use App\Repository\CategoryRepository;
use Mockery\CountValidator\Exception;
use App\Http\Requests\User\AddCategoryRequest;
use App\Http\Requests\User\EditCategoryRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\ApiController;

class CategoryController extends ApiController
{

  protected $categoryRepository;
  public function __construct()
  {
    parent::__construct();
    $this->categoryRepository = app(CategoryRepository::class);
  }
  public function index()
  {
    // checkUser;
    try {
      return $this->categoryRepository->getCategoryFromUser(1); // Auth::id()
     } catch (Exception $e) {
         return response()->json(['error' => $e->getMessage()], 500);
     }
  }  

  public function store(AddCategoryRequest $request)
  {
    // checkUser;
    try {
      $arrParam = $request->all();
      $arrParam['user_id'] = 1;
      $category = $this->categoryRepository->create($arrParam); // Auth::id()
      return $this->buildResponse(true, $category);

     } catch (Exception $e) {
         return response()->json(['error' => $e->getMessage()], 500);
     }
  }

  public function destroy($id)
  {
    try{
      $this->categoryRepository->delete($id);
      return response()->json(['success' => true]);
    }catch(Exception $e){
      return response()->json(['error' => $e->getMessage()], 500); 
    }
  }

  public function edit($id)
  {
    try{
      $category =  $this->categoryRepository->detail($id);
      return response()->json(['category' => $category]);
    }catch(Exception $e){
      return response()->json(['error' => $e->getMessage()], 500); 
    }
  }
  public function update(EditCategoryRequest $request, $id)
  {
    try{
      $category =  $this->categoryRepository->update($id, $request->all());
      return response()->json(['success' => true]);
    }catch(Exception $e){
      return response()->json(['error' => $e->getMessage()], 500); 
    }
  }
 
  // private function checkUser($user_id)
  // {
  //     $user = $this->userRepository->detail($user_id);
  //     if(empty($user_id)) return abort(404);
  //     return $user;
  // }
}
