<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

class ApiController extends Controller
{

    public function __construct()
    {
        $this->resolvePaginationCurrentPage();
    }

    /**
     * Build json response for api request
     *
     * @param bool $status
     * @param array $result
     * @param array $extraResponse
     *
     * @return bool
     */
    public function buildResponse($status, $result = null, $extraResponse = [])
    {
        $response = [
            'status' => Response::HTTP_OK,
            'message' => $this->getMessage(Response::HTTP_OK),
            'success' => $status
        ];

        if (is_array($extraResponse)) {
            $response = array_merge($response, $extraResponse);
        }

        if (! is_null($result)) {
            $response['data'] = $result;
        }

        // Return debug info when enable debug on .env file
        if (env('APP_DEBUG', false) && $debugBar = app('debugbar')) {
            $response['debug'] = $debugBar->collect();
        }

        $headerStatus = $this->getHeaderResponseStatusCode($response['status']);

        return response()->json($response, $headerStatus);
    }

    /**
     * Get header response status code
     *
     * @param int $statusCode
     *
     * @return int
     */
    public function getHeaderResponseStatusCode($statusCode)
    {
        return $this->checkErrorCodeIsAppError($statusCode)
            ? floor($statusCode / 100000)
            : $statusCode;
    }

    /**
     * Check error code is app error
     *
     * @param int $errorCode
     *
     * @return bool
     */
    public function checkErrorCodeIsAppError($errorCode)
    {
        return $errorCode > 1000 ? true : false;
    }

    /**
     * Failure json response
     *
     * @param int $errorCode
     * @param string $errorMessage
     * @param array $extra
     *
     * @return bool
     */
    public function failResponse($errorCode = null, $errorMessage = '', $extra = [])
    {
        if (!$errorMessage) {
            $errorMessage = $this->getMessage($errorCode);
        }

        $response = [
            'status' => $errorCode,
            'message' => $errorMessage
        ];

        if (!empty($extra)) {
            $response['errors'] = $extra;
        }

        return $this->buildResponse(false, null, $response);
    }

    /**
     * Not validate json response
     *
     * @param array $errors
     * @return bool
     */
    public function notValidateResponse($errors = [])
    {
        return $this->failResponse(Response::HTTP_BAD_REQUEST, null, $errors);
    }

    /**
     * Build success response json
     *
     * @param array $data
     * @param array $extra
     *
     * @return bool
     */
    public function succeedResponse($data = [], $extra = null)
    {
        return $this->buildResponse(true, $data, $extra);
    }

    /**
     * Build success response json for pagination data
     *
     * @param object $pagination
     * @param string $dataKey
     * @param array $extra
     *
     * @return bool
     */
    public function succeedPaginationResponse($pagination, $dataKey = 'items', $extra = [])
    {
        $pagination->setPath('/' . app()->make('request')->path());

        $tmpResult = $pagination->toArray();

        $data = $tmpResult['data'];

        unset($tmpResult['data']);

        $result['pagination'] = $tmpResult;

        $result[$dataKey] = $data;

        if (!empty($extra) && is_array($extra)) {
            $result = array_merge($result, $extra);
        }

        return $this->buildResponse(true, $result);
    }

    /**
     * Get error message
     *
     * @param int $code
     *
     * @return string
     */
    public function getMessage($code)
    {
        return isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '';
    }

    /**
     * Get pagination per page
     */
    public function getPaginationPerPage()
    {
        $requestPerPage = (int) app()->make('request')->input('per_page');

        return $requestPerPage ? $requestPerPage : 50;
    }

    /**
     * Auto resolve pagination current page by request param current_page
     */
    public function resolvePaginationCurrentPage()
    {
        Paginator::currentPageResolver(function () {
            $request = app('request');
            return $request->input('current_page', $request->input('page', 1));
        });
    }


    /**
     * Check is Debug mode
     *
     * @return bool
     */
    public function isDebugMode()
    {
        if (app()->environment('local', 'testing') && (int) app()->make('request')->input('debug') == 1) {
            return true;
        }

        return false;
    }
}
