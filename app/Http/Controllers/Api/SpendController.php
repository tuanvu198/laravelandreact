<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Repository\SpendRepository;
use App\Repository\CategoryRepository;
use Mockery\CountValidator\Exception;
use App\Http\Requests\User\AddSpendRequest;
use App\Http\Requests\User\EditSpendRequest;
use Illuminate\Support\Facades\Auth;

class SpendController extends Controller
{
  protected $spendRepository;
  protected $categoryRepository;
  public function __construct()
  {
    $this->spendRepository    = app(SpendRepository::class);
    $this->categoryRepository = app(CategoryRepository::class);
  }
  public function index()
  {
    // checkUser;
    
    try {
      return $this->spendRepository->querySpend(1); // Auth::id()
     } catch (Exception $e) {
         return response()->json(['error' => $e->getMessage()], 500);
     }
  }  
  public function edit($id)
  {
    try {
      $spend =  $this->spendRepository->detail($id);
      $categories = $this->categoryRepository->getCategoryFromUser(1);
      return response()->json(['spend' => $spend, 'categories' => $categories]);
  } catch (Exception $e) {
    return response()->json(['error' => $e->getMessage()], 500);
   }
  }
  public function update(EditSpendRequest $request, $id)
    {
       try{
         $arrParam                =  $request->all();
         $arrParam['user_id']     = 1; // auth()->user()->id
         $arrParam['category_id'] = $request->category;

         $spend = $this->spendRepository->update($id,$arrParam);

         return response()->json(['success' => true]);
       }catch(Exception $e){
         return response()->json(['error' => $e->getMessage()], 500); 
       }
    }

    public function store(AddSpendRequest $request)
    {
       try{
         $arrParam            = $request->all();
         $arrParam['user_id'] = 1; // auth()->user();
         $spend = $this->spendRepository->create($arrParam);
         
         return response()->json(['spend' => $spend]);
       }catch(Exception $e){
         return response()->json(['error' => $e->getMessage()], 500); 
       }
    }
    public function destroy($id)
    {
      try{
        $this->spendRepository->delete($id);
        return response()->json(['success' => true]);
      }catch(Exception $e){
        return response()->json(['error' => $e->getMessage()], 500); 
      }
    }

  // private function checkUser($user_id)
  // {
  //     $user = $this->userRepository->detail($user_id);
  //     if(empty($user_id)) return abort(404);
  //     return $user;
  // }
}
