<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Repository\CategoryRepository;
use App\Http\Requests\Admin\AddCategoryRequest;
use App\Http\Requests\Admin\EditCategoryRequest;
use App\Repository\UserRepository;

class CategoryController extends Controller
{
    protected $categoryRepository;
    protected $userRepository;
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->userRepository     = app(UserRepository::class);
    }

    public function index($user_id)
    {
        $this->checkUser($user_id);
        $categories = $this->categoryRepository->getCategoryFromUser($user_id);

        return view('admin.categories.index',['categories' => $categories,'user_id'=> $user_id]);

    }
    public function edit($user_id, $category_id)
    {
        $this->checkUser($user_id);
        $category = $this->categoryRepository->detail($category_id);
        if(empty($category)) return abort(404);
        return view('admin.categories.edit',['category' => $category,'user_id'=>$user_id]);
    }
    public function update($user_id, $id, EditCategoryRequest $request)
    {
      try{
          $this->checkUser($user_id);
          $this->categoryRepository->update($id,$request->all());
          return redirect()->route('admin.categories.index',['user_id'=>$user_id])->with('alert-success', trans('messages.success_updated'));
      }catch (\Exception $e){
          Log::error('==================ERROR_UPDATE_CATEGORIES=========================='.$e->getMessage());
          return abort(500);
      }
    }

    public function create($user_id)
    {
        $this->checkUser($user_id);
        return view('admin.categories.create',['user_id'=> $user_id]);
    }
    public function store($user_id,AddCategoryRequest $request)
    {
        try{
            $this->checkUser($user_id);
            $params = $request->all();
            $params['user_id'] = $user_id;
            $this->categoryRepository->create($params);
            return redirect()->route('admin.categories.index',['user_id' => $user_id])->with('alert-success', trans('messages.success_created'));

        }catch(\Exception $e){
            Log::error('==================ERROR_ADD_CATEGORIES=========================='.$e->getMessage());
            return abort(500);
        }

    }
    public function delete($id)
    {
        try{
            $this->categoryRepository->delete($id);
            return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
        }catch(\Exception $e){
            Log::error('==================ERROR_DELETE_CATEGORIES=========================='.$e->getMessage());
            return abort(500);
        }
    }
    private function checkUser($user_id)
    {
        $user = $this->userRepository->detail($user_id);
        if(empty($user)){
            return abort(404);
        }
        return $user;
    }

}
