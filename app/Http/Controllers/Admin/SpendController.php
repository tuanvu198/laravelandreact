<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\EditSpendRequest;
use App\Model\Spend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Repository\CategoryRepository;
use App\Http\Requests\Admin\AddSpendRequest;
use App\Http\Requests\Admin\EditCategoryRequest;
use App\Repository\UserRepository;
use App\Repository\SpendRepository;
use Mockery\CountValidator\Exception;

class SpendController extends Controller
{
    protected $categoryRepository;
    protected $spendRepository;
    protected $userRepository;

    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->spendRepository    = app(SpendRepository::class);
        $this->userRepository     = app(UserRepository::class);
    }

    public function index($category_id)
    {
        try {
            $spends = $this->spendRepository->getSpendFromCat($category_id);
            $category = $this->categoryRepository->detail($category_id);
            return view('admin.spends.index', ['spends' => $spends, 'category' => $category]);
        }
        catch (\Exception $e){
            Log::error('=========ERROR===SHOW==============SPEND',$e->getMessage());
            return abort(500);
         }
    }
    public function create($category_id)
    {
        try{
            $category = $this->categoryRepository->detail($category_id);
            return view('admin.spends.create',['category' => $category]);
        }catch (\Exception $e){
            Log::error('=========ERROR===CREATE==SPEND',$e->getMessage());
            return abort(500);
        }

    }
    public function store(AddSpendRequest $request,$cateogy_id,$user_id)
    {
        try{
            $arrParam = $request->all();
            $arrParam['user_id'] =  $user_id;
            $this->spendRepository->create($arrParam);
            return redirect()->route('admin.spends.index',['category_id' => $cateogy_id])->with('alert-success', trans('messages.success_created'));
        }catch (\Exception $e){
            Log::error('========ERRORS===STOGE===SPEND',$e->getMessage());
            return abort(500);
        }

    }
    public function edit($category_id,$spend_id)
    {
        try{
            $spend = $this->spendRepository->detail($spend_id,['categories']);
            if(empty($spend)) return abort(404);
            return view('admin.spends.edit',['category_id'=> $category_id,'spend' => $spend]);
        }catch (\Exception $e){
            Log::error('====EDIT_SPEND===',$e->getMessage());
            return abort(500);
        }
    }
    public function update($category_id,$spend_id, EditSpendRequest $request)
    {
        try{
             $spend = $this->spendRepository->detail($spend_id,['categories']);
             $arrParam = $request->all();
             $arrParam['user_id']     = $spend->user_id;
             $arrParam['category_id'] = $spend->category_id;
             $this->spendRepository->update($spend_id,$arrParam);
             return redirect()->route('admin.spends.index',['category_id' => $category_id])->with('alert-success', trans('messages.success_updated'));
        }catch (\Exception $e){
            Log::error('=======ERROR-UPDATE--SPEND=====',$e->getMessage());
            return abort(500);
        }
    }
    public function delete($category_id,$spend_id)
    {
        try{
            $this->spendRepository->delete($spend_id);
            return redirect()->route('admin.spends.index',['category_id' => $category_id])->with('alert-success', trans('messages.success_deleted'));
        }catch (\Exception $e){
            Log::error('=====DELETE_SPEND=========',$e->getMessage());
            return abort(500);
        }
    }
    public function statistic($user_id)
    {
        try{
            $this->checkUser($user_id);
            $categories = $this->categoryRepository->getCategoryFromUser($user_id);
            $spends     = $this->spendRepository->getSpendFromUser($user_id);
            return view('admin.spends.statistic',['categories' => $categories,'spends' => $spends,'user_id' => $user_id]);
        }catch (\Exception $e){
            Log::error('===========SPEND_STATISTIC===========',$e->getMessage());
            return abort(500);
        }
    }
    public function statisticType(Request $request,$category_id)
    {
        if(isset($request->type)){
             $type = $request->type;
             if ($type == Spend::RECEIVE || $type == Spend::BUY){
               $spends = $this->spendRepository->getTypeStatistic($category_id,$type);
                 return view('admin.spends.statistic-type',['spends' => $spends,'category_id'=>$category_id]);
             }else{
                 return abort(500);
             }
        }

    }
    public function filterStatistic(Request $request,$user_id)
    {
        $this->checkUser($user_id);
        /*
         * PARAMER FILTER
         * */
        $filter    = $request->filter;
        $from_date = $request->from_date;
        $to_date   = $request->to_date;
        $value_category = $request->filter_category;
        /*
         * / PARAMMER FILTER
         * */
        $categories = $this->categoryRepository->getCategoryFromUser($user_id);
        $spends     = $this->spendRepository->filterSpendFromUserAndDate($user_id,$filter,array('from_date' => $from_date,'to_date'=> $to_date,'vl_category' => $value_category));
        return view('admin.spends.statistic',['categories' => $categories,'spends' => $spends,'user_id' => $user_id]);
    }
    private function checkUser($user_id)
    {
        $user = $this->userRepository->detail($user_id);
        if(empty($user_id)) return abort(404);
        return $user;
    }
   
}
