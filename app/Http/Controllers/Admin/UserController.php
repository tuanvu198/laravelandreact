<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AddUserRequest;
use App\Http\Requests\Admin\EditPasswordRequest;
use App\Http\Requests\Admin\EditUserRequest;
use App\Jobs\ActiveUserJob;
use App\Jobs\ChangePassword;
use App\Repository\ProfileRepository;
use App\Repository\UserRepository;
use App\Services\UserServices;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    protected $repository;
    protected $profileRepository;
    protected $services;

    public function __construct()
    {
        $this->repository = app(UserRepository::class);
        $this->profileRepository = app(ProfileRepository::class);

        $this->services = app(UserServices::class);
    }

    public function index()
    {
        try {
            $users = $this->repository->paginate(PAGINATION_PER_PAGE, ['profile', 'role']);
            return view('admin.users.index', ['users' => $users]);
        } catch (\Exception $exception) {
            Log::error('ERROR_LIST_USERS'.$exception->getMessage());
            return abort(500);
        }


    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(AddUserRequest $request)
    {
        DB::beginTransaction();
        try {
            $params = $request->all();
            $params['token_active'] = generateToken();
            $params['password'] = bcrypt($params['password']);
            $user = $this->repository->create($params);

            ActiveUserJob::dispatch($user)->onQueue('mails');

            $params['user_id'] = $user->id;
            $params['image']   =  $this->services->uploadImage($request->file('image'));

            $this->profileRepository->create($params);
            DB::commit();
            return redirect()->route('admin.users.index')->with('alert-success', trans('messages.success_created'));


        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('==================ERROR_ADD_USERS=========================='.$exception->getMessage());
            return abort(500);
        }
    }

    public function edit($id)
    {
        $user = $this->repository->detail($id, ['profile']);

        if (empty($user)) {
            return abort(404);
        }

        return view('admin.users.edit', ['user' => $user]);
    }

    public function editPassword($id)
    {
        $user = $this->repository->detail($id, ['profile']);

        if (empty($user)) {
            return abort(404);
        }

        return view('admin.users.edit-password', ['user' => $user]);
    }

    public function updatePassword($id, EditPasswordRequest $request)
    {
        try {
            $user = $this->repository->detail($id);

            if (empty($user)) {
                return abort(404);
            }

            if (auth()->user()->email !== $user->email) {
                ChangePassword::dispatch($user, $request->input('new_password'))->onQueue('mails');
            }


            $this->repository->update($user->id,['password' => bcrypt($request->input('new_password'))]);
            return redirect()->back()->with('alert-success', trans('messages.updated_success'));

        } catch (\Exception $exception) {
            Log::error('==================ERROR_EDIT_PASSWORD_USERS=========================='.$exception->getMessage());
            return abort(500);
        }


    }

    public function update($id, EditUserRequest $request)
    {
      try{
          $user = $this->repository->detail($id, ['profile']);

          if (empty($user)) {
              return abort(404);
          }

          $params = $request->all();
          $params['email'] = $user->email;


      }catch (\Exception $e){
          Log::error('=========EDIT_USER========',$e->getMessage());
      }
    }

    public function delete($id)
    {
      try{
          $this->repository->delete($id);
          return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
      }catch (\Exception $e){
         Log::error('===========ERROR_DELETE_USER===============', $e->getMessage());
         return abort(404);
      }
    }
}
