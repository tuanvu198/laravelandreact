<?php
namespace App\Repository;

use App\Model\User;

class UserRepositoryEloquent extends BasicRepositoryEloquent implements UserRepository
{
    public function __construct()
    {
        $this->model = app(User::class);
    }
}
