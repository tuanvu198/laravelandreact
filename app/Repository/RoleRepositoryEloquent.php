<?php
namespace App\Repository;

use App\Model\Profile;
use App\Model\Role;

class RoleRepositoryEloquent extends BasicRepositoryEloquent implements RoleRepository
{
    public function __construct()
    {
        $this->model = app(Role::class);
    }
}
