<?php
namespace App\Repository;

use App\Model\Category;

class CategoryRepositoryEloquent extends BasicRepositoryEloquent implements CategoryRepository
{
    public function __construct()
    {
        $this->model = app(Category::class);
    }
    public function getCategoryFromUser(int $id)
    {
       return $this->model->where('user_id',$id)->paginate(PAGINATION_PER_PAGE);
    }

}
