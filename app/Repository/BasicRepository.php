<?php
namespace App\Repository;

interface BasicRepository {
    public function paginate(int $perPage, array $relation, $column = ['*']);

    public function detail(int $id, array $relation);

    public function all();
}
