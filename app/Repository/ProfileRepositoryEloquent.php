<?php
namespace App\Repository;

use App\Model\Profile;

class ProfileRepositoryEloquent extends BasicRepositoryEloquent implements ProfileRepository
{
    public function __construct()
    {
        $this->model = app(Profile::class);
    }
    
}
