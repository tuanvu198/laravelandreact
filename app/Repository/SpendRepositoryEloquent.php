<?php
namespace App\Repository;

use App\Model\Spend;
use Illuminate\Support\Facades\DB;
class SpendRepositoryEloquent extends BasicRepositoryEloquent implements SpendRepository
{
    public function __construct()
    {
        $this->model = app(Spend::class);
    }
    public function getSpendFromUser(int $user_id)
    {
       return $this->model->where('user_id',$user_id)->get();
    }
    public function filterSpendFromUserAndDate(int $user_id,string $filter,$option = null)
    {
        $day = 0;
        switch ($filter){
            case 'yesterday':
                $day = -Spend::YESTERDAY;
                break;
            case 'today':
                $day = Spend::TODAY;;
                break;
            case 'week':
                $day = -Spend::WEEK;
                break;
            case 'month':
                return $this->model->where('user_id',$user_id)->whereMonth('created_at',date("m"))->get();
                break;
            case 'clear':
                return $this->model->where('user_id',$user_id)->get();
                break;
            case 'receive':
                return $this->model->where('user_id',$user_id)->where('type',Spend::RECEIVE)->get();
                break;
            case 'buy':
                return $this->model->where('user_id',$user_id)->where('type',Spend::BUY)->get();
                break;
            case 'date':
                if($option['from_date'] !== null && $option['to_date'] !== null) return $this->model->where('user_id',$user_id)->whereBetween('created_at', [$option['from_date'], $option['to_date']])->get();
                break;
            case 'category':
                if(!empty($option['vl_category'])){
                    return $this->model->where('user_id',$user_id)->where('category_id',$option['vl_category'])->get();
                }
                break;
            default: return $day;
        }

        $startTime = date("Y-m-d");
        $convertedTime = date('Y-m-d',strtotime($day.'day',strtotime($startTime)));
        return $this->model->where('user_id',$user_id)->whereDate('created_at', $convertedTime)->get();
    }
    public function getSpendFromCat(int $cat_id)
    {
        return $this->model->where('category_id',$cat_id)->get();
    }
    public function getTypeStatistic(int $category_id, int $type )
    {
      return $this->model->where('category_id',$category_id)->where('type',$type)->get();
    }

    /**
     * 
     * SHOW LIST SPEND FRONT END
     */
    public function querySpend($user_id)
    {
     return  $this->model->select('categories.name as category','spends.updated_at','spends.total','spends.id', DB::raw("case WHEN spends.type = 1 then 'Thu nhập' else 'Tiền tiêu' end as type"))->leftJoin('categories', 'categories.id', '=', 'spends.category_id')->where('spends.user_id',$user_id)->latest('updated_at')->paginate(PAGINATION_PER_PAGE);
    }
    
}
