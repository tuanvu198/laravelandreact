<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//// BUILD API
Route::get('/api/{path}', function(){
   return view('layouts/app');
})->where('path','.*');



Route::group(['namespace' => 'Auth'], function () {
    Route::get('logout', 'LogoutController@logout')->name('auth.logout');
    Route::get('active/{token}', 'ActiveUserController@active')->name('auth.active');
    
    Route::get('auth/facebook', 'FacebookController@redirectToFacebook');
    Route::get('auth/facebook/callback', 'FacebookController@handleFacebookCallback');
});
Route::namespace('Admin')->group(function () {
    Route::prefix('admin')->group(function () {
        Route::middleware(['auth', 'admin'])->group(function () {
            Route::name('admin.')->group(function () {
                Route::get('/', 'AdminController@index')->name('index');
                Route::prefix('users')->group(function () {
                    Route::name('users.')->group(function () {
                        Route::get('/', 'UserController@index')->name('index');
                        Route::get('/create', 'UserController@create')->name('create');
                        Route::post('/create', 'UserController@store')->name('store');
                        Route::get('/edit/{id}', 'UserController@edit')->name('edit');
                        Route::put('/edit/{id}', 'UserController@update')->name('update');
                        Route::get('/edit-password/{id}', 'UserController@editPassword')->name('edit.password');
                        Route::put('/edit-password/{id}', 'UserController@updatePassword')->name('update.password');
                        Route::delete('/delete/{id}', 'UserController@delete')->name('delete');
                    });
                    Route::prefix('{user_id}')->group(function () {
                        Route::prefix('categories')->group(function () {
                            Route::name('categories.')->group(function () {
                                Route::get('/', 'CategoryController@index')->name('index');
                                Route::get('/create', 'CategoryController@create')->name('create');
                                Route::post('/create', 'CategoryController@store')->name('store');
                                Route::get('/edit/{id}', 'CategoryController@edit')->name('edit');
                                Route::put('/edit/{id}', 'CategoryController@update')->name('update');
                                Route::delete('/delete/{id}', 'CategoryController@delete')->name('delete');
                            });
                        });
                    });
                    Route::prefix('categories/{category_id}')->group(function () {
                        Route::prefix('spends')->group(function (){
                            Route::name('spends.')->group(function (){
                                Route::get('/','SpendController@index')->name('index');
                                Route::get('create','SpendController@create')->name('create');
                                Route::post('create/{user_id}','SpendController@store')->name('store');
                                Route::get('edit/{id}','SpendController@edit')->name('edit');
                                Route::put('edit/{id}','SpendController@update')->name('update');
                                Route::delete('/delete/{id}', 'SpendController@delete')->name('delete');
                                Route::get('statistic-type','SpendController@statisticType')->name('statistic.type');
                            });
                        });
                    });
                });
                Route::prefix('users/{user_id}')->group(function (){
                    Route::prefix('spends')->group(function (){
                        Route::name('spends.')->group(function (){
                            Route::get('/','SpendController@statistic')->name('statistic');
                            Route::match(['get', 'post'],'filter','SpendController@filterStatistic')->name('filter');
                        });
                    });
                });

            });
        });
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
