<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::namespace('Api')->group(function () {

    Route::group([
        'prefix' => 'auth'

    ], function ($router) {

        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');

    });
    Route::group([
        'prefix' => 'auth'

    ], function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });
    
    Route::prefix('v1')->group(function () {
        Route::group(['prefix' => 'spend'], function ($router) {
            Route::get('/', 'SpendController@index');
            Route::post('/add', 'SpendController@store');
            Route::get('/edit/{id}', 'SpendController@edit');
            Route::put('/edit/{id}', 'SpendController@update');
            Route::delete('/delete/{spend_id}', 'SpendController@destroy');
        });

        Route::group(['prefix' => 'category'], function ($router) {
            Route::get('/', 'CategoryController@index');
            Route::post('/add', 'CategoryController@store');

            Route::get('/edit/{id}', 'CategoryController@edit');
            Route::put('/edit/{id}', 'CategoryController@update');
            
            Route::delete('/delete/{id}', 'CategoryController@destroy');
        });
    });
   
 
});
