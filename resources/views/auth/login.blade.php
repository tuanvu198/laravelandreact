@extends('admin.layouts.mini')
@section('content')

    <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
            <div class="col-lg-4 mx-auto">
                @include('admin.component.alert')
                @include('admin.component.errors')
                <div class="auto-form-wrapper">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label class="label">Địa chỉ email</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="email" placeholder="Email">
                                <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label">Mật khẩu</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" placeholder="*********">
                                <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary submit-btn btn-block">Đăng nhập</button>
                        </div>
                        <div class="form-group d-flex justify-content-between">
                            <div class="form-check form-check-flat mt-0">
                                <label class="form-check-label">
                                    <input type="checkbox" name="remember" class="form-check-input" {{ old('remember') ? 'checked' : '' }}> Giữ trạng thái đăng nhập
                                </label>
                            </div>
                            <a href="{{ route('password.request') }}" class="text-small forgot-password text-black">Quên mật khẩu</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- content-wrapper ends -->

@endsection

