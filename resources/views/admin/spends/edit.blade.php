@extends('admin.layouts.app')

@section('title', 'Chỉnh sửa chi tiêu')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <p class="card-description mb-5">
                        Chi tiêu
                        <code>Thêm chi tiêu</code>
                    </p>
                    <form class="forms-sample" method="post" action="{{route('admin.spends.update', ['category_id' => $category_id,'spend_id'=> $spend->id])}}" >
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="type">Chọn kiểu: &nbsp;</label>
                            <label>Thu nhập
                                <input type="radio" {{ ($spend->type == \App\Model\Spend::RECEIVE ) ? "checked" : '' }} name="type" value="{{\App\Model\Spend::RECEIVE}}">
                            </label>
                            <label>Chi tiêu
                                <input type="radio" {{ ($spend->type == \App\Model\Spend::BUY ) ? "checked" : '' }} name="type" value="{{\App\Model\Spend::BUY}}">
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="total">Tổng tiền: </label>
                            <input type="text" class="form-control"  value="{{$spend->total}}" name="total" placeholder="Nhập số tiền !">
                        </div>
                        <div class="form-group">
                            <label for="created_at">Ngày: </label>
                            <input type="text" class="form-control"  id="datepicker" value="{{ $spend->created_at }}" name="created_at" placeholder="Chọn ngày !">
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Lưu</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
@endsection