@extends('admin.layouts.app')

@section('title', 'Chi tiêu')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý chi tiêu</h4>
                    <p class="card-description mb-5">
                        <span>Danh mục</span>
                        <code>{{$category->name}}</code>
                        <a href="{{route('admin.spends.create', ['category_id' => $category->id])}}" class="btn btn-secondary float-right">Thêm chi tiêu</a>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th>
                                    STT
                                </th>
                                <th>Type</th>
                                <th>Ngày tạo</th>
                                <th>Tổng tiền</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $stt =1;?>
                            @forelse($spends as $spend)
                                <tr>
                                    <td>{{ $stt }}</td>
                                    <td>
                                        <a href="{{ route('admin.spends.statistic.type',['category_id' => $category->id]).'?type='.$spend->type }}">
                                            {{ $spend->type_format }}
                                        </a>
                                    </td>
                                    <td>
                                        {{$spend->created_at_format}}
                                    </td>
                                    <td>
                                        <code>{{$spend->total_format}}</code>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.spends.edit',['category_id' => $category->id,'spend_id' => $spend->id]) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                    </td>
                                    <td>
                                        <form action="{{ route('admin.spends.delete',['category_id' =>$category->id,'spend_id' => $spend->id]) }}" class="form-edit formConfirmDeleteSpend" method="post">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                        </form>
                                    </td>
                                </tr>
                                <?php $stt++;?>
                            @empty
                                <tr>
                                    <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $('form.formConfirmDeleteSpend .submitDelete').click(function () {
            $('form.formConfirmDeleteCat').submit();
        })
    </script>
    @endsection
