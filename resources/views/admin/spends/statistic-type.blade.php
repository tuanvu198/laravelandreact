@extends('admin.layouts.app')

@section('title', 'Chi tiêu')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý chi tiêu</h4>
                    <p class="card-description mb-5">
                        <span>Danh mục</span>
                        <code>Thống kê chi tiêu</code>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th>
                                    STT
                                </th>
                                <th>Type</th>
                                <th>Ngày tạo</th>
                                <th>Tổng tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $stt =1; $total = 0;?>
                            @forelse($spends as $spend)
                                <tr>
                                    <td>{{ $stt }}</td>
                                    <td>{{ $spend->type_format }}</td>
                                    <td>
                                        {{$spend->created_at_format}}
                                    </td>
                                    <td>
                                        <code>{{$spend->total_format}}</code>
                                    </td>
                                </tr>
                                <?php $stt++; $total += $spend->total?>
                            @empty
                                <tr>
                                    <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="total">
                        <h4>Tổng số tiền là:&nbsp; <span class="lable label-success">{{ number_format($total). ' vnđ' }}</span></h4>
                        <a class="btn btn-success float-right" href="{{route('admin.spends.index',['category_id' => $category_id])}}">Quay lại</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

