@extends('admin.layouts.app')

@section('title', 'Thêm chi tiêu')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <p class="card-description mb-5">
                        Chi tiêu
                        <code>Thêm chi tiêu</code>
                    </p>
                    <form class="forms-sample" method="post" action="{{route('admin.spends.store', ['category_id' => $category->id,'user_id' => $category->user->id])}}" >
                        @csrf
                        <div class="form-group">
                            <label for="type">Chọn kiểu: &nbsp;</label>
                            <label>Thu nhập
                                <input type="radio" checked name="type" value="{{\App\Model\Spend::RECEIVE}}">
                            </label>
                            <label>Chi tiêu
                                <input type="radio" name="type" value="{{\App\Model\Spend::BUY}}">
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="type">Danh mục</label>
                            <select class="form-control" name="category_id">
                                <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="total">Tổng tiền: </label>
                            <input type="text" class="form-control"  value="{{old('total')}}" name="total" placeholder="Nhập số tiền !">
                        </div>
                        <div class="form-group">
                            <label for="created_at">Ngày: </label>
                            <input type="text" class="form-control"  id="datepicker" value="{{old('created_at')}}" name="created_at" placeholder="Chọn ngày !">
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Lưu</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
@endsection