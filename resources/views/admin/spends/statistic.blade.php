@extends('admin.layouts.app')

@section('title', 'Chi tiêu')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý chi tiêu</h4>
                    <p class="card-description mb-5">
                        <span>Thống kê</span>
                        <code>Chi tiêu</code>
                    </p>
                    <div class="statistic">
                        <div class="filters border border-primary" style="padding: 15px; margin-bottom: 20px;">
                            <button class="btn btn-success rounded">Lọc dữ liệu</button>

                            <a class="btn btn-info btn-xs" href="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=yesterday'}}">Hôm qua</a>
                            <a class="btn btn-info btn-xs" href="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=today'}}">Hôm nay</a>

                            <a class="btn btn-info btn-xs" href="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=week'}}">Tuần này</a>

                            <a class="btn btn-info btn-xs" href="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=month'}}">Tháng này</a>

                            <a class="btn btn-info btn-xs" href="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=receive'}}">Thu nhập</a>
                            <a class="btn btn-info btn-xs" href="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=buy'}}">Chi tiêu</a>

                            <a class="btn btn-info btn-xs" href="javascript:void(0)"  data-toggle="modal" data-target="#modalFilterDay">Chọn ngày</a>
                            <a class="btn btn-info btn-xs" href="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=clear'}}"><b>Không lọc</b></a>
                        </div>
                        <div class="filter_category_for_spends">
                            <form action="{{ route('admin.spends.filter',['user_id'=> $user_id]).'?filter=category' }}" method="POST" id="category">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <div class="form-group">
                                    <label for="statistic">Thống kê chi tiêu:</label>
                                    <select name="filter_category" class="form-control" id="filter_category">
                                        <option value="0">@lang('messages.categories.option')</option>
                                        @forelse($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name}}</option>
                                        @empty
                                            <option>@lang('messages.categories.no_data')</option>
                                        @endforelse
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th>Stt</th>
                                    <th>Ngày</th>
                                    <th>Danh mục</th>
                                    <th>Tổng tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $stt = 1; $buy = 0; $receive = 0;?>
                                @forelse($spends as $spend)
                                    @if($spend->type === \App\Model\Spend::BUY) <?php $buy += $spend->total  ?>
                                        @else <?php $receive += $spend->total  ?>
                                    @endif
                                <tr>
                                    <td>{{$stt}}</td>
                                    <td>{{ $spend->created_at_format }}</td>
                                    <td>{{ $spend->categories->name }}</td>
                                    <td class="{{(\App\Model\Spend::RECEIVE === $spend->type) ? 'text-success' : 'text-danger'}}">{{$spend->total_format}}</td>
                                </tr>
                                    <?php $stt++; ?>
                                @empty
                                    @lang('messages.no_data')
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="cost" style="margin-top: 50px;">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th class="bg-success rounded-top">TỔNG THU NHẬP</th>
                                    <th class="bg-danger rounded-top">TỔNG CHI TIÊU</th>
                                    <th class="bg-warning rounded-top">SỐ DƯ</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td class="text-success">{{ number_format($receive)}} vnđ</td>
                                    <td class="text-danger">-{{number_format($buy)}} vnđ</td>
                                    <td class="text-warning">{{number_format($receive - $buy)}} vnđ</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalFilterDay" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalFilterDay">Lọc dữ liệu theo ngày</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.spends.filter',['user_id' => $user_id]).'?filter=date'}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="created_at">Từ ngày: </label>
                            <input type="text" class="form-control datepicker" required   value="{{old('from_date')}}" name="from_date" >
                        </div>
                        <div class="form-group">
                            <label for="created_at">Dến ngày: </label>
                            <input type="text" class="form-control datepicker" required  value="{{old('to_date')}}" name="to_date">
                        </div>

                        <div ass="row">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Đồng ý</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $('#filter_category').change(function () {
            $('form#category').submit();
        })
        $( function() {
            $( ".datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
        } );
    </script>
    @endsection
