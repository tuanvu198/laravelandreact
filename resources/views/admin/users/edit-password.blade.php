@extends('admin.layouts.app')

@section('title', 'Sửa mật khẩu')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <p class="card-description mb-5">
                        Tài khoản
                        <code>Thông tin tài khoản</code>
                        <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-primary pull-right">Sửa tài khoản</a>
                    </p>
                    <form class="forms-sample" method="post" action="{{route('admin.users.update.password',['id' => $user->id])}}" enctype="multipart/form-data" id="edit_user">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="email">Địa chỉ email</label>
                            <input type="email" disabled class="form-control" id="email" value="{{$user->email}}" name="email" placeholder="Nhập địa chỉ email" >
                        </div>
                        <div class="form-group">
                            <label for="new_password">Nhập mật khẩu mới</label>
                            <input type="password" class="form-control" id="new_password"  name="new_password" placeholder="Nhập mật khẩu mới" >
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Nhập lại mật khẩu</label>
                            <input type="password" class="form-control" id="confirm_password" name="new_password_confirmation" placeholder="Nhập lại mật khẩu mới" >
                        </div>

                        <div class="form-group">
                            <label for="role">Hình ảnh</label>
                            <img src="{{asset('storage/users/avatar/'.(isset($user->profile->image) ? $user->profile->image : ''))}}" alt="{{isset($user->profile->last_name) ? $user->profile->last_name : ''}}" class="img img-responsive img-avatar" style="max-width: 300px;margin-bottom: 20px;">
                        </div>
                        <button type="submit" class="btn btn-success mr-2" >Chỉnh sửa</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
