@extends('admin.layouts.app')

@section('title', 'Thông tin tài khoản')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <p class="card-description mb-5">
                        Tài khoản
                        <code>Thông tin tài khoản</code>
                        <a href="{{route('admin.users.edit.password', $user->id)}}" class="btn btn-primary pull-right">Sửa mật khẩu</a>
                    </p>
                    <form class="forms-sample" method="post" action="{{route('admin.users.update',['id' => $user->id])}}" enctype="multipart/form-data" id="edit_user">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="email">Địa chỉ email</label>
                            <input type="email" disabled class="form-control" id="email" value="{{$user->email}}" name="email" placeholder="Nhập địa chỉ email" >
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="first-name">Họ</label>
                                    <input type="text" class="form-control" id="first-name" value="{{isset($user->profile->first_name) ? $user->profile->first_name : ''}}" name="first_name" placeholder="Hãy nhập họ" >
                                </div>
                                <div class="col-md-6">
                                    <label for="last-name">Tên</label>
                                    <input type="text" class="form-control" id="last-name" value="{{isset($user->profile->last_name) ? $user->profile->last_name : ''}}" name="last_name" placeholder="Hãy nhập tên" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role">Quyền truy cập</label>
                            <select class="form-control" id="role" name="role_id" >
                                <option value="">Chọn quyền truy cập</option>
                                <option value="{{\App\Model\Role::ROLE_ADMIN}}" {{\App\Model\Role::ROLE_ADMIN == $user->role_id ? 'selected' : ''}}>@lang('messages.role.admin')</option>
                                <option value="{{\App\Model\Role::ROLE_MEMBER}}" {{\App\Model\Role::ROLE_MEMBER == $user->role_id ? 'selected' : ''}}>@lang('messages.role.member')</option>
                                <option value="{{\App\Model\Role::ROLE_CUSTOMER}}" {{\App\Model\Role::ROLE_CUSTOMER == $user->role_id ? 'selected' : ''}}>@lang('messages.role.customer')</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role">Giới tính</label>
                            <select class="form-control" id="sex" name="sex" >
                                <option value="{{\App\Model\Profile::SEX_MALE}}" {{isset($user->profile->sex) && \App\Model\Profile::SEX_MALE == $user->profile->sex ? 'selected' : ''}}>Nam</option>
                                <option value="{{\App\Model\Profile::SEX_FEMALE }}" {{isset($user->profile->sex) && \App\Model\Profile::SEX_FEMALE == $user->role_id ? 'selected' : ''}}>Nữ</option>
                                <option value="{{\App\Model\Profile::SEX_UNKNOWN}}" {{isset($user->profile->sex) && $user->sex == \App\Model\Profile::SEX_UNKNOWN ? 'selected' : ''}}>Không khai báo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role">Hình ảnh</label>
                            <img src="{{asset('storage/users/avatar/'.(isset($user->profile->image) ? $user->profile->image : ''))}}" alt="{{isset($user->profile->last_name) ? $user->profile->last_name : ''}}" class="img img-responsive img-avatar" style="max-width: 300px;margin-bottom: 20px;">
                            <input type="file" class="form-control" name="image" >
                        </div>
                        <button type="submit" class="btn btn-success mr-2" >Chỉnh sửa</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
