@extends('admin.layouts.app')

@section('title', 'Tài khoản')

{{--@section('sidebar')--}}
{{--@parent--}}

{{--<p>This is appended to the master sidebar.</p>--}}
{{--@endsection--}}

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <p class="card-description mb-5">
                        Danh sách
                        <code>Tài khoản</code>
                        <a href="{{route('admin.users.create')}}" class="btn btn-secondary float-right">Thêm tài khoản</a>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Họ tên
                                </th>
                                <th>
                                    Giới tính
                                </th>
                                <th>
                                    Quyền
                                </th>
                                <th>
                                    Trạng thái
                                </th>
                                <th>Ngày tạo</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)
                            <tr>
                                <td><a href="{{route('admin.users.edit', $user->id)}}">{{$user->id}}</a></td>
                                <td>
                                    <a href="{{route('admin.users.edit', $user->id)}}">
                                    {{isset($user->profile->full_name) ? $user->profile->full_name : USER_EMPTY_PROFILE}}
                                    </a>
                                </td>
                                <td>
                                    {{isset($user->profile->sex_format) ? $user->profile->sex_format : USER_EMPTY_PROFILE}}
                                </td>
                                <td>
                                    {{$user->role->name}}
                                </td>
                                <td>
                                    {{$user->status_format}}
                                </td>
                                <td>
                                    {{$user->created_at}}
                                </td>
                                <td>
                                    <a href="{{ Route('admin.categories.index',['user_id' => $user->id]) }}"  data-toggle="tooltip" title="Xem danh mục !" class="btn btn-info"><i class="fas fa-indent"></i></a>
                                </td>
                                <td>
                                    <a href="{{ Route('admin.spends.statistic',['user_id' => $user->id]) }}" class="btn btn-primary" data-toggle="tooltip" title="Xem chi tiêu !"><i class="fas fa-hand-holding-usd"></i></a>
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
