@extends('admin.layouts.app')

@section('title', 'Thêm tài khoản mới')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <p class="card-description mb-5">
                        Tài khoản
                        <code>Thêm tài khoản</code>
                    </p>
                    <form class="forms-sample" method="post" action="{{route('admin.users.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="email">Địa chỉ email</label>
                            <input type="email" class="form-control" id="email" value="{{old('email')}}" name="email" placeholder="Nhập địa chỉ email">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="first-name">Họ</label>
                                    <input type="text" class="form-control" id="first-name" value="{{old('first_name')}}" name="first_name" placeholder="Hãy nhập họ">
                                </div>
                                <div class="col-md-6">
                                    <label for="last-name">Tên</label>
                                    <input type="text" class="form-control" id="last-name" value="{{old('last_name')}}" name="last_name" placeholder="Hãy nhập tên">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Nhập mật khẩu</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Nhập mật khẩu">
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Nhập mật khẩu</label>
                            <input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Nhập lại mật khẩu">
                        </div>
                        <div class="form-group">
                            <label for="role">Quyền truy cập</label>
                            <select class="form-control" id="role" name="role_id">
                                <option value="">Chọn quyền truy cập</option>
                                <option value="{{\App\Model\Role::ROLE_ADMIN}}" {{\App\Model\Role::ROLE_ADMIN == old('role_id') ? 'selected' : ''}}>@lang('messages.role.admin')</option>
                                <option value="{{\App\Model\Role::ROLE_MEMBER}}" {{\App\Model\Role::ROLE_MEMBER == old('role_id') ? 'selected' : ''}}>@lang('messages.role.member')</option>
                                <option value="{{\App\Model\Role::ROLE_CUSTOMER}}" {{\App\Model\Role::ROLE_CUSTOMER == old('role_id') ? 'selected' : ''}}>@lang('messages.role.customer')</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role">Giới tính</label>
                            <select class="form-control" id="sex" name="sex">
                                <option value="{{\App\Model\Profile::SEX_UNKNOWN}}">Không khai báo</option>
                                <option value="{{\App\Model\Profile::SEX_MALE}}" {{\App\Model\Profile::SEX_MALE == old('sex') ? 'selected' : ''}}>Nam</option>
                                <option value="{{\App\Model\Profile::SEX_FEMALE }}" {{\App\Model\Profile::SEX_FEMALE == old('role_id') ? 'selected' : ''}}>Nữ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role">Hình ảnh</label>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Gửi</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
