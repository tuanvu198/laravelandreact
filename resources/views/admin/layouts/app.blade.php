<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Quản lí chi tiêu - @yield('title')</title>
  <link rel="stylesheet" href="{{asset('template/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('template/admin/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{asset('template/admin/vendors/css/vendor.bundle.addons.css')}}">
  <link rel="stylesheet" href="{{asset('template/admin/css/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('template/admin/images/favicon.png')}}" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" >
  <link rel="stylesheet" href="{{asset('lib/jquery-ui/jquery-ui.min.css')}}">

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('admin.component.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
       @include('admin.component.sidebar')
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
            @include('admin.component.alert')
            @include('admin.component.errors')
            @yield('content')
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
           <!-- Load Facebook SDK for JavaScript -->
            <div id="fb-root"></div>
            <script>
               window.fbAsyncInit = function() {
                FB.init({
                  appId            : '327306991189614',
                  autoLogAppEvents : true,
                  xfbml            : true,
                  version          : 'v2.12'
                });
              };
            (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

            <!-- Your customer chat code -->
            <div class="fb-customerchat"
            attribution=setup_tool
            page_id="220520838535108">
        </div>
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <script src="{{asset('template/admin/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{asset('template/admin/vendors/js/vendor.bundle.addons.js')}}"></script>
  <script src="{{asset('template/admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('template/admin/js/misc.js')}}"></script>
  <script src="{{asset('template/admin/js/dashboard.js')}}"></script>
  <script src="{{asset('lib/jquery-ui/jquery-ui.min.js')}}"></script>
  <script type="text/javascript">
    $(function () {
      $('.flash-message').hide(5000);
    })
  </script>
  @yield('script')
</body>

</html>
