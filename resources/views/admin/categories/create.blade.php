@extends('admin.layouts.app')

@section('title', 'Thêm thể loại mới')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <p class="card-description mb-5">
                        Thể loại
                        <code>Thêm thể loại </code>
                    </p>
                    <form class="forms-sample" method="post" action="{{route('admin.categories.store', ['user_id' => $user_id])}}" >
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" value="{{old('name')}}" name="name" placeholder="Nhập danh mục !">
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Gửi</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
