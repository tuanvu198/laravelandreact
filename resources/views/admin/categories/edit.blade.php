@extends('admin.layouts.app')

@section('title', 'Thể loại')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Danh mục</h4>
                    <p class="card-description mb-5">
                        chỉnh sửa
                        <code>{{ $category->name }}</code>
                    </p>
                    <form class="forms-sample" method="post" action="{{route('admin.categories.update',['id' => $category->id,'user_id'=> $user_id])}}" >
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Tên</label>
                            <input type="text" class="form-control" id="name" value="{{$category->name}}" name="name" placeholder="Nhập danh mục !">
                        </div>

                        <button type="submit" class="btn btn-success mr-2" >Chỉnh sửa</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
