@extends('admin.layouts.app')

@section('title', 'Danh mục')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Danh mục</h4>
                    <p class="card-description mb-5">
                        Danh sách
                        <code>danh mục</code>
                        <a href="{{route('admin.categories.create',['user_id'=>$user_id])}}" class="btn btn-secondary float-right">Thêm danh mục</a>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>Tên</th>
                                <th>Ngày tạo</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $stt =1;?>
                            @forelse($categories as $category)
                                <tr>
                                    <td>{{ $stt }}</td>
                                    <td>
                                        <a data-toggle="tooltip" title="Xem chi tiêu !"  href="{{route('admin.spends.index',['id' => $category->id])}}">
                                            {{ $category->name }}
                                        </a>
                                    </td>
                                    <td>
                                        {{$category->created_at_format}}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.categories.edit',['id' => $category->id,'user_id' => $user_id]) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                    </td>
                                    <td>
                                        <form action="{{route('admin.categories.delete',['user_id' => $user_id,'id'=>$category->id])}}" class="form-edit formConfirmDeleteCat" method="post">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-danger submitDelete" data-toggle="modal" data-target="#myModalDelete" value="Xóa">
                                        </form>
                                    </td>
                                </tr>
                                <?php $stt++;?>
                            @empty
                                <tr>
                                    <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $('form.formConfirmDeleteCat .submitDelete').click(function () {
            $('form.formConfirmDeleteCat').submit();
        })
    </script>
    @endsection
