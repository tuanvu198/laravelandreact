import React, { Component } from 'react';
export default class ErrorAlert extends Component {
    constructor(props)
    {
        super(props);
        this.renderErrors = this.renderErrors.bind(this);
    }
    renderErrors() {
        const  errors  = this.props;

         const itemErr = errors.message.map( (err, key) => {
           if(key == 0){
            return (
              <li className="alert alert-danger flash-message">
                { err }
              </li>
            )
           }
            
        });
        return  itemErr ;
        
      };
     
    render() {
        const errros = this.renderErrors();
        console.log(errros)
      
        return (
              <div>
               { errros }
              </div>
        );
    }
}
