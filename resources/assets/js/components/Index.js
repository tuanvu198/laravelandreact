import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Redirect, Route } from "react-router-dom";
import Header from './Header';
import Footer from './Footer';
import Login from './auth/Login';
export default class Index extends Component {
    render() {
        // let login = localStorage.getItem('access_token');
        // if (!login) {
        //     console.log('chua login duoc va bi redict');
        //     return (
        //             <div>
        //                 <Redirect to='/login'/>
        //                 <Route path='/login' component={ Login }/>
        //             </div>
    
        //     )
        // }
        return (
            <div className="wrapper">
               <div className="container">
                <Header />
                <Footer />
               </div>
                
            </div>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Router><Index /></Router>, document.getElementById('app'));
}
