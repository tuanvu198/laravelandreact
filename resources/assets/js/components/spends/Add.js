import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import DatePicker   from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import CustomInputDate  from './../notifications/CustomInputDate';
import Axios from 'axios';
// import {   Route, Link, Switch } from "react-router-dom";
/**
 * NOTIFICATION
 */
import SuccessAlert from './../notifications/SuccessAlert';
import ErrorAlert from './../notifications/ErrorAlert';
export default class Add extends Component {
    constructor(props){
        super(props);
        this.state = {
            type: null,
            category_id: null,
            total      : null,
            created_at : new Date(),
            categories : [],
            alert_mess : '',
            errors     : '',

        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
    }
    componentDidMount(){
      Axios.get('/api/v1/category').
      then(res => {
          this.setState({
            categories : res.data.data
          })
 
      }).catch(err => {

      })
    }
    handleChangeDate(date){
        this.setState({
            created_at: date
        })
    }
    handleSubmit(event){
        event.preventDefault();
  
        let uri  = '/api/v1/spend/add';
        var value = {
                     category_id: this.state.category_id,
                     total: this.state.total,
                     created_at: this.state.created_at,
                     type: this.state.type
                    }
        Axios.post(uri,value)
        .then( res => {
            console.log(res);
            this.setState({
                alert_mess : 'success',
            })
        })
        .catch(err => {
            if (err.response.status == 422) {
                this.setState({
                    alert_mess: 'error',
                    errors: err.response.data.errors
                })
                console.log(err.response);
            }
        });
    }
    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        
        this.setState({
        [name]: value
        });
      }
    render() {
        var { total, categories, category_id, created_at, type, errors,alert_mess } = this.state;
        return (
            <div>
              <div className="row">
                 <hr />
              
              { (this.state.alert_mess === "success" ? <SuccessAlert message={"Thêm mới thành công !"} /> : null)}
                    <div className="col-lg-12 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Tài khoản</h4>
                                <p className="card-description mb-5">
                                    Chi tiêu
                                    <code>Thêm chi tiêu</code>
                                </p>
                                <form onSubmit={ this.handleSubmit }>
                                   <div className="form-group">
                                        <label for="type">Chọn kiểu: &nbsp;</label>
                                        <label>Thu nhập
                                            <input type="radio" onChange={this.handleChange} name="type" value="1" />
                                        </label>
                                        <label>Chi tiêu
                                            <input type="radio" onChange={this.handleChange}  name="type" value="2" />
                                        </label>
                                    </div>
                                    { (alert_mess == "error" &&  errors.type != null) ?  <ErrorAlert message={errors.type} /> : ' '} 
                                    <div class="form-group">
                                    <label for="sel1">Danh mục</label>
                                    <select class="form-control" name="category_id" id="sel1" onChange={this.handleChange}>
                                     <option value={0}>Chọn danh mục</option>
                                    {
                                        categories.map((category,index) => {
                                            return (
                                                <option value={ category.id}>{ category.name}</option>
                                            )
                                        })
                                    }
                                        
                                    </select>
                                    { ( alert_mess == "error" &&  errors.category_id != null) ?  <ErrorAlert message={errors.category_id} /> : ' '} 
                                    </div>
                                    <div className="form-group">
                                        <label for="total">Tổng tiền: </label>
                                        <input type="text" onChange={this.handleChange} className="form-control"  value={total } name="total"  />
                                    </div>
                                    { ( alert_mess == "error" && errors.total != null) ?  <ErrorAlert message={errors.total} /> : ' '} 
                                    <div className="form-group">
                                        <label for="created_at">Ngày tạo :</label>
                                        <DatePicker
                                            dateFormat="dd/MM/yyyy"
                                            customInput = {<CustomInputDate date={created_at} />}
                                            selected={created_at}
                                            onChange={this.handleChangeDate}
                                            
                                        />
                                    </div>
                                    { ( alert_mess == "error" &&  errors.created_at != null) ?  <ErrorAlert message={errors.created_at} /> : ' '} 
                                    <button type="submit" className="btn btn-success mr-2">Lưu</button>
                                </form>

                            </div>
                        </div>
                    </div>
              </div>
            </div>
        );
    }
}

 