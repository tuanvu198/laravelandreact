import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {  Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import SuccessAlert from './../notifications/SuccessAlert';
export default class List extends Component {
    constructor(props){
        super(props);
        this.state = {
            listSpend: [],
            activePage: 1,
            itemsCountPerPage: 1,
            totalItemsCount: 1,
            pageRangeDisplayed:3,
            alert_mess: ''
        }
        this.handlePageChange = this.handlePageChange.bind(this);
    }
    componentWillMount() {
        axios.get('/api/v1/spend',
         ).then( (response) => {
             this.setState({
                 listSpend: response.data.data
             })
          })
        .catch( error => {
            // handle error
            console.log(error.response);
          })
         
    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        axios.get("/api/v1/spend?page=" + pageNumber)
        .then(res => {
            console.log(res)
            this.setState({
                listSpend: res.data.data,
                activePage : res.data.current_page,
                itemsCountPerPage: res.data.per_page,
                totalItemsCount:res.data.total,
            });
        })
      }
      onDelete(id){
        
       axios.delete('/api/v1/spend/delete/' + id)
       .then( res => {
           console.log(res);
           var spends = this.state.listSpend;
           for(var i = 0; i < spends.length; i++)
           {
              if(spends[i].id == id)
              {
                  spends.splice(i,1);
                  this.setState({listSpend:spends})
              }
           }
           this.setState({
                 alert_mess : "success"
           })
       }).catch(error => {
            this.setState({
                alert_mess : "error"
        })
       })
      }

    render() {
        // console.log(this.state);
        return (
            <div className="row">
            <div className="col-sm-12">
             <Link className="btn btn-success" to="/api/spend/add">Thêm chi tiêu</Link>
             { (this.state.alert_mess === "success" ? <SuccessAlert message={"Dữ liệu đã được xóa thành công !"} /> : null)}
                  <div className="table-responsive">
                            <table className="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Stt</th>
                                    <th>Ngày</th>
                                    <th>Danh mục</th>
                                    <th>Loại</th>
                                    <th>Tổng tiền</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>         
                                {
                                    this.state.listSpend.map( (spend, index) => {
                                        return (                      
													<tr>
														<td key={index}>{ spend.id }</td>
														<td>{ spend.updated_at }</td>
														<td>{ spend.category }</td>
														<td>{ spend.type }</td>
                                                        <td>{ spend.total }</td>
														<td>
														<Link className="btn btn-info" to={ `/api/spend/edit/${spend.id}`} >Edit</Link> &nbsp;
														<button className="btn btn-danger" onClick={ this.onDelete.bind(this,spend.id)}>Delete</button>
														</td>
													</tr>
                                    )
                                })
                                }
                                </tbody>
                            </table>
                       <div>
                       <div className="d-flex justify-content-center">

                        <Pagination
                        
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemsCountPerPage}
                            totalItemsCount={this.state.totalItemsCount}
                            pageRangeDisplayed={this.state.pageRangeDisplayed}

                            onChange={this.handlePageChange}
                            itemClass = 'page-item'
                            linkClass = 'page-link'
                        />
                    </div>
                    </div>
                   </div>
                         
              </div>
            </div>
        );
    }
}
 