import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import DatePicker   from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import CustomInputDate  from './../notifications/CustomInputDate';
/**
 * NOTIFICATION
 */
import SuccessAlert from './../notifications/SuccessAlert';
import ErrorAlert from './../notifications/ErrorAlert';

export default class Edit extends Component {
    constructor(props){
        super(props);
        console.log(props);
        this.state = {
            'category' : '',
            'type'        :  '',
            'total'       : 0,
            'categories'  : [],
            'created_at'  : new Date()
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
    }
    componentDidMount(){
      let { id }  = this.props.match.params;
      let uri  = '/api/v1/spend/edit/' + id;
      axios.get(uri)
      .then( res => {
        var data = res.data;
        this.setState({
            'category'    : data.spend.category_id,
            'type'        : data.spend.type,
            'created_at'  : data.spend.created_at,
            'total'       : data.spend.total,
            'categories'  : data.categories,
             alert_mess    : '',
             errors        : [],
        })
      })
      .catch(err => {
          console.log(err.response);
      });
    }
    handleSubmit(e){
        e.preventDefault();
        
        let { id }  = this.props.match.params;
        let uri  = '/api/v1/spend/edit/' + id;
        console.log(this.state);
     axios.put(uri, this.state)
          .then(res => {
            this.setState({
                alert_mess : 'success'
            })
          })
          .catch(err => {
            if (err.response.status == 422) {
                this.setState({
                    alert_mess: 'error',
                    errors: err.response.data.errors
                })
            }
          })
    }
    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
        [name]: value
        });
      }
     handleChangeDate(date){
         this.setState({
             created_at: date
         })
     }
      

    render() {
        var { type, categories, total, created_at, alert_mess, errors } = this.state;
        return ( 
            <div>
                 <div className="row">
                 <hr />
              { (this.state.alert_mess === "error" ? <ErrorAlert message={this.state.errors} /> : null)}
              { (this.state.alert_mess === "success" ? <SuccessAlert message={"Cập nhật dữ liệu thành công !"} /> : null)}
                    <div className="col-lg-12 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Tài khoản</h4>
                                <p className="card-description mb-5">
                                    Chi tiêu
                                    <code>Chỉnh sửa chi tiêu</code>
                                </p>
                                <form onSubmit={ this.handleSubmit }>
                                   <div className="form-group">
                                        <label for="type">Chọn kiểu: &nbsp;</label>
                                        <label>Thu nhập
                                            <input type="radio" onChange={this.handleChange} checked={ type == 1  ? "ckecked" : null} name="type" value="1" />
                                        </label>
                                        <label>Chi tiêu
                                            <input type="radio" onChange={this.handleChange} checked={ type == 2  ? "ckecked" : null} name="type" value="2" />
                                        </label>
                                    </div>
                                    { (alert_mess == "error" &&  errors.type != null) ?  <ErrorAlert message={errors.type} /> : ' '} 
                                    <div class="form-group">
                                         <label for="sel1">Danh mục</label>
                                        <select class="form-control" name="category" id="sel1" onChange={this.handleChange}>
                                        {
                                            categories.map((category,index) => {
                                                return (
                                                    <option value={ category.id}>{ category.name}</option>
                                                )
                                            })
                                        }
                                            
                                        </select>
                                    </div>
                                    { (alert_mess == "error" &&  errors.category != null) ?  <ErrorAlert message={errors.category} /> : ' '} 
                                    <div className="form-group">
                                        <label for="total">Tổng tiền: </label>
                                        <input type="text" onChange={this.handleChange} className="form-control"  value={total } name="total"  />
                                    </div>
                                    { (alert_mess == "error" &&  errors.total != null) ?  <ErrorAlert message={errors.total} /> : ' '} 
                                    <div className="form-group">
                                        <label for="created_at">Ngày tạo :</label>
                                        <DatePicker
                                            dateFormat="dd/MM/yyyy"
                                            customInput = {<CustomInputDate date={created_at} />}
                                            selected={created_at}
                                            onChange={this.handleChangeDate}
                                            
                                        />
                                    </div>
                                    <button type="submit" className="btn btn-success mr-2">Lưu</button>
                                </form>

                            </div>
                        </div>
                    </div>
            </div>
            </div>
        );
    }
}
 
 