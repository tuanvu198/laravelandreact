import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import List from './List';
import Edit from './Edit';
import Add from './Add';
import {  Switch, Link, Route } from "react-router-dom";
export default class Spend extends Component {
    render() {
        return (
            <div className="spend">
               
                <Route exact= {true} path="/api/spend" component={ List } />
                <Route path="/api/spend/edit/:id"  component= { Edit }/>
                <Route path="/api/spend/add"  component= { Add }/>
            </div>
        );
    }
}

 