import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import Contact from './Contact';
import Category from './categories/Index';
import Spend    from './spends/Index';
import homePage from './homePage';
import Notify   from './Notify';
import {   Route, Link, Switch } from "react-router-dom";
export default class Header extends Component {
    render() {
        return (
            <div className="page">
               <div className="login">
                  This is a login
               </div>
                <div className="panel panel-primary">
                    <div className="panel-heading">
                       <div className="nav">
                          <ul>
                            <li><Link exact={true} to="/api/">Trang chu</Link></li>
                            <li><Link to="/api/spend">Thu chi</Link></li>
                            <li><Link to="/api/category">The loai</Link></li>
                            <li><Link to="/api/notify">Thong bao</Link></li>
                          </ul>             
                       </div>
                    </div>
                </div>
                <Switch>
                    <Route exact path="/api/" component={homePage} />
                    <Route path="/api/spend" component={ Spend } />
                    <Route path="/api/category" component={ Category } />
                    <Route path="/api/notify" component={ Notify } />
                </Switch>
            </div>
        );
    }
}

 