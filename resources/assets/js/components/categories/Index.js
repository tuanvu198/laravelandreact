import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import List from './List';
import Add from './Add';
import Edit from './Edit';
 
import {   Route, Link, Switch } from "react-router-dom";
export default class Category extends Component {
    render() {
        return (
            <div id="category">
                <Route exact={true} path="/api/category" component={ List }></Route>
                <Route  path="/api/category/add" component={ Add }></Route>
                <Route  path="/api/category/edit/:id" component={ Edit }></Route>
            </div>
        );
    }
}

 