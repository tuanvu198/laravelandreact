import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {  Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import SuccessAlert from './../notifications/SuccessAlert';
export default class List extends Component {
    constructor(props){
        super(props);
        this.state = {
            categories: [],
            activePage: 1,
            itemsCountPerPage: 1,
            totalItemsCount: 1,
            pageRangeDisplayed:3,
            alert_mess: ''
        }
        this.handlePageChange = this.handlePageChange.bind(this);
    }
    componentDidMount() {
        axios.get('/api/v1/category',
         ).then( (response) => {

             this.setState({
                categories: response.data.data
             })
          })
        .catch( error => {
            // handle error
            console.log(error.response);
          })
         
    }
    // chua xu ly su kien
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        axios.get("/api/v1/category?page=" + pageNumber)
        .then(res => {
            console.log(res)
            this.setState({
                categories: res.data.data,
                activePage : res.data.current_page,
                itemsCountPerPage: res.data.per_page,
                totalItemsCount:res.data.total,
            });
        })
      }
      onDelete(id){
        
       axios.delete('/api/v1/category/delete/' + id)
       .then( res => {
           console.log(res);
           var { categories } = this.state;
           for(var i = 0; i < categories.length; i++)
           {
              if(categories[i].id == id)
              {
                  categories.splice(i,1);
                  this.setState({categories:categories})
              }
           }
           this.setState({
                 alert_mess : "success"
           })
       }).catch(error => {
            this.setState({
                alert_mess : "error"
        })
       })
      }

    render() {
        console.log(this.state);
        return (
            <div className="row">
              <div className="col-sm-12">
             <Link className="btn btn-success" to="/api/category/add">Thêm danh muc</Link>
             { (this.state.alert_mess === "success" ? <SuccessAlert message={"Dữ liệu đã được xóa thành công !"} /> : null)}
                  <div className="table-responsive">
                            <table className="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Thu nhập</th>
                                    <th>Chi tiêu</th>
                                    <th>Số dư</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>         
                                { 
                                    this.state.categories.map( (category, index) => {
                                        return (                      
													<tr>
														<td key={index}>{ category.name }</td>
														<td>{ category.updated_at }</td>
											 
														<td>{ category.name }</td>
                                                        <td>d</td>
														<td>
														<Link className="btn btn-info" to={ `/api/category/edit/${category.id}`} >Edit</Link> &nbsp;
														<button className="btn btn-danger" onClick={ this.onDelete.bind(this,category.id)}>Delete</button>
														</td>
													</tr>
                                       )
                                })
                                } 
                                </tbody>
                            </table>
                       <div>
                       <div className="d-flex justify-content-center">

                        <Pagination
                        
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemsCountPerPage}
                            totalItemsCount={this.state.totalItemsCount}
                            pageRangeDisplayed={this.state.pageRangeDisplayed}

                            onChange={this.handlePageChange}
                            itemClass = 'page-item'
                            linkClass = 'page-link'
                        />
                    </div>
                    </div>
                   </div>
                   </div>
            </div>
        );
    }
}
 