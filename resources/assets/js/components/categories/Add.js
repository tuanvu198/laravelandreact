import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
/**
 * NOTIFICATION
 */
import SuccessAlert from './../notifications/SuccessAlert';
import ErrorAlert from './../notifications/ErrorAlert';
export default class Add extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            alert_mess : '',
            errors     : [],

        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleSubmit(event){
        event.preventDefault();
  
        let uri  = '/api/v1/category/add';
        var value = {
                     name : this.state.name
                    }
        Axios.post(uri,value)
        .then( res => {
            console.log(res);
            this.setState({
                alert_mess : 'success',
            })
        })
        .catch(err => {
            if (err.response.status == 422) {
                this.setState({
                    alert_mess: 'error',
                    errors: err.response.data.errors
                })
    
            }
        });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        
        this.setState({
        [name]: value
        });
      }
    render() {
        var { name, errors , alert_mess} = this.state;
        return (
            <div>
              <div className="row">
                 <hr />
              { (this.state.alert_mess === "success" ? <SuccessAlert message={"Thêm mới thành công !"} /> : null)}
                    <div className="col-lg-12 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Tài khoản</h4>
                                <p className="card-description mb-5">
                                    Danh mục
                                    <code>Thêm danh mục</code>
                                </p>
                                <form onSubmit={ this.handleSubmit }>
                                   
                                    <div className="form-group">
                                        <label for="total">Tên danh mục: </label>
                                        <input type="text" onChange={this.handleChange} className="form-control"  value={name } name="name"  />
                                    </div>
                                    
                                    { (alert_mess == "error" && errors.name !== null) ?  <ErrorAlert message={errors.name} /> : ' '} 
                                        
                                    <button type="submit" className="btn btn-success mr-2">Lưu</button>
                                </form>

                            </div>
                        </div>
                    </div>
              </div>
            </div>
        );
    }
}

 