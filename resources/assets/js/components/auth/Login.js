import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';
export default class Login extends Component {
    constructor(){
        super();
        this.state = {
            'email': '',
            'password' : '',
            'remember' : true
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(e){
      var target = e.target;
      var name   = target.name;
      var value  = target.type === 'checkbox' ? target.checked : target.value;

      this.setState({
        [name]: value
        });
    }
    handleSubmit(e){
     e.preventDefault();
     let uri = '/api/auth/login';
     axios.post(uri, this.state)
     .then( (response) => {
          console.log(response);
     })
     .catch( errors => {
         console.log(errors.response);
     })
     
     console.log(this.state);
    }
    render() {
        return(
            <div className="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
            <div className="row w-100">
                <div className="col-lg-4 mx-auto">

                    <div className="auto-form-wrapper">
                        <form onSubmit={this.handleSubmit} action="#">

                            <div className="form-group">
                                <label className="label">Địa chỉ email</label>
                                <div className="input-group">
                                    <input type="text" onChange={this.handleChange} className="form-control" name="email" placeholder="Email" />
                                    <div className="input-group-append">
                                    <span className="input-group-text">
                                        <i className="mdi mdi-check-circle-outline"></i>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="label">Mật khẩu</label>
                                <div className="input-group">
                                    <input type="password" onChange={this.handleChange} className="form-control" name="password" placeholder="*********" />
                                    <div className="input-group-append">
                                    <span className="input-group-text">
                                        <i className="mdi mdi-check-circle-outline"></i>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary submit-btn btn-block">Đăng nhập</button>
                            </div>
                            <div className="form-group d-flex justify-content-between">
                                <div className="form-check form-check-flat mt-0">
                                    <label className="form-check-label">
                                        <input type="checkbox" onChange={this.handleChange} name="remember" className="form-check-input" /> Giữ trạng thái đăng nhập
                                    </label>
                                </div>
                                <a href="#" className="text-small forgot-password text-black">Quên mật khẩu</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
 
        )
       
    }
}

 